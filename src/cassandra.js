const cassandra = require('cassandra-driver')

const client = new cassandra.Client({ contactPoints: ['127.0.0.1'], keyspace: 'demo' })

client.execute("CREATE TABLE users (lastname text PRIMARY KEY, age int, city text, email text, firstname text)", function (err, result) {
  console.log('yo')
  if (!err) {
    console.log('results', results)
  } else {
    console.log('Table not created')
  }
})

client.execute("SELECT lastname, age, city, email, firstname FROM users WHERE lastname='Jones'", function (err, result) {
  if (!err) {
    if (result.rows.length > 0) {
      const user = result.rows[0]
      console.log("name = %s, age = %d", user.firstname, user.age)
    } else {
      console.log("No results")
    }
  }
})

client.execute("UPDATE users SET age = 36 WHERE lastname = 'Jones'", function (err, result) {
})

client.execute("SELECT firstname, age FROM users where lastname = 'Jones'", function (err, result) {
  const user = result.rows[0]
  console.log("name = %s, age = %d", user.firstname, user.age)
})

client.execute("DELETE FROM users WHERE lastname = 'Jones'", function (err, result) {
  if (!err) {
    console.log("Deleted")
  }
})

client.execute("SELECT * FROM users WHERE lastname='Jones'", function (err, result) {
  if (result.rows.length > 0) {
    const user = result.rows[0]
    console.log("name = %s, age = %d", user.firstname, user.age)
  } else {
    console.log("No records")
  }
})

process.exit()